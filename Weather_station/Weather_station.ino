#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <DHT.h>

#define wifi_ssid "SkyFibre"
#define wifi_password "REDACTED"

#define mqtt_server "192.168.0.58"
#define mqtt_user "homeassistant"
#define mqtt_password "2032"
#define mqtt_client "waether_station"

#define SLEEP_DELAY_IN_SECONDS 5 //21600

#define humidity_topic "sensor/outdoor/humidity"
#define temperature_topic "sensor/outdoor/temperature"

#define DHTTYPE DHT11
#define DHTPIN 5

WiFiClient espClient;
PubSubClient client(espClient);
DHT dht(DHTPIN, DHTTYPE, 11);

void setup() {
  Serial.begin(115200);
  dht.begin();
  setup_wifi();
  client.setServer(mqtt_server, 1883);
}

void setup_wifi() {
  delay(10);
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(wifi_ssid);

  WiFi.begin(wifi_ssid, wifi_password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}


void reconnect() {
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection... ");
    if (client.connect(mqtt_client, mqtt_user, mqtt_password)) {
      Serial.println("connected");
    } else {
      Serial.print("Failed to connect, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

bool checkBound(float newValue, float prevValue, float maxDiff) {
  return !isnan(newValue) &&
         (newValue < prevValue - maxDiff || newValue > prevValue + maxDiff);
}

long lastMsg = 0;
float temp = 0.0;
float hum = 0.0;
float diff = 1.0;
void readTempHumidity() {
  long now = millis();
//  if (now - lastMsg > 2000) {
    lastMsg = now;

//    float newTemp = dht.readTemperature();
//    float newHum = dht.readHumidity();

//    if (checkBound(newTemp, temp, diff)) {
      temp = 30; //newTemp;
      Serial.print("New temperature:");
      Serial.println(String(temp).c_str());
      client.publish(temperature_topic, String(temp).c_str(), true);
//    }

//    if (checkBound(newHum, hum, diff)) {
      hum = 24; //newHum;
      Serial.print("New humidity:");
      Serial.println(String(hum).c_str());
      client.publish(humidity_topic, String(hum).c_str(), true);
//    }
//  }
}

void loop() {
  if (!client.connected()) {
    Serial.print("Not connected to MQTT. State: ");
    Serial.println(client.state());
    reconnect();
  }
  client.loop();

>>>>>>> 48763749a49463173897e43871e42aa31e0bf8e4
  readTempHumidity();

  Serial.print("Entering deep sleep mode for ");
  Serial.print(SLEEP_DELAY_IN_SECONDS);
  Serial.println(" seconds...");
  ESP.deepSleep(SLEEP_DELAY_IN_SECONDS * 1000000, WAKE_RF_DEFAULT);
}
